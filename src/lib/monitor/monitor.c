/**
 *****************************************************************************
 * @title   Console.c
 * @author  Rainer Becker
 * @date    02.Apr. 2013
 * @brief   The console will  be used for testing the software and hardware
 *******************************************************************************/

/* Includes ------------------------------------------------------------------*/

#include <stdbool.h>
#include <string.h>

#include "stm32f0xx.h"
#include "stm32f0xx_rcc.h"
#include "stm32f0xx_gpio.h"
#include "stm32f0xx_misc.h"
#include "stm32f0xx_usart.h"

#include "FreeRTOS.h"

#include "monitor.h"
#include "spi.h"

#include "conversions.h"
#include "logger.h"
#include "usart.h"
#include "conversions.h"
#include "spi_host_com.h"
#include "os.h"


/* Private typedef -----------------------------------------------------------*/


/* Private define ------------------------------------------------------------*/
#define RXBUFFERSIZE   (80)
#define PROMPT          "\n\r-> "
#define INVALID_CMD     "\n\r??"

typedef enum {
	CMD_CPU_BURN = 'c',
	CMD_INFO = 'i',
	CMD_SRAM_DUMP = 'd',
	CMD_HELP = 'h',
	CMD_GET_TIME= 'g',
	CMD_SET_TIME= 't',
	CMD_SEND_THREE_BYTES = 's',
	CMD_RESET = 'r',
} CMD_MONITOR_T;

// special chars
#define BLANK ' '
#define TAB '\t'

// answer for editor
#define YES 'y'
#define NO 'n'

// ident specialization
#define BYTE_ID 'b'
#define WORD_ID 'w'
#define LONG_ID 'l'
#define REGISTER 'r'
#define INPUT 'i'
#define OUTPUT 'o'
#define PNP 'p'
#define NPN 'n'
#define PULLUP 'u'
#define PULLDOWN 'd'
#define TRISTATE 't'
#define IDENT_ERROR 'e'
// End of editor definitions

#define SRAM_SIZE 0x2000

/* Private macro -------------------------------------------------------------*/
#define countof(a)   (sizeof(a) / sizeof(*(a)))

/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
static void information();
static void reset_ioc();

/* Private functions ---------------------------------------------------------*/

/**
 * @brief Displays the version like hardware version and other information like all active background tasks on monitor
 * @retval none
 * @see  log_msg() get_hardware_version() get_firmware_version() get_chip_id() xPortGetFreeHeapSize() cpu_burn_get_bg_iterations() mram_test_get_bg_iterations()
 * @see flash_test_is_background_active() tc_test_is_background_active() adc_test_is_background_active()
 */

static void information()
{
    log_msg("Heap left          : %d", xPortGetFreeHeapSize());
    log_msg("Ticks since start  : %d", xTaskGetTickCount());
    log_msg("Running tasks      : %d", uxTaskGetNumberOfTasks());

    xTaskHandle tcb = os_tcb_next(NULL);
    while (tcb)
    {
        log_msg("    %s", pcTaskGetTaskName(tcb));
        log_msg("        prio       : %d", uxTaskPriorityGet(tcb));
        log_msg("        stack left : %d words", uxTaskGetStackHighWaterMark(tcb));
        tcb = os_tcb_next(tcb);
    }

    log_msg("");
}




/**
 * @brief Makes an hardware reset on the ioc
 * @see NVIC_SystemReset()
 */
static void reset_ioc()
{
    NVIC_SystemReset();
}


/**
 * @brief Makes SRAM dump from address start to end.
 *
 * @param   start   start address to use for dump; may have any value
 *                  between 0 and SRAM_SIZE
 * @param   end     end address to use for dump; may have any value
 *                  between 0 and SRAM_SIZE and should be bigger than start
 *
 * @return  none
 */
void sram_dump(const uint8_t* start, const uint8_t* end)
{
    bool new_line = true;
    uint8_t column = 0;
    const uint8_t* i = start;
    while (i < end)
    {
        if (new_line)
        {
            log_msg("");
            log_msg("%x: ", i);
            new_line = false;
        }
        log_msg("%02x ", *i);
        column++;

        if (column >= 16)
        {
            column = 0;
            new_line = true;

        }
        ++i;
    }
    log_msg("");
}


/**
 * @brief Makes an dump output from the sram from a start point to end point
 * @param buf the input buffer from uart as char stream with blank as delimiter
 * @retval none
 * @see util_atox() log_msg()
 */
void sram_dump_parse(char *buf)
{
    char* saveptr;
    /* in RXBuffer at position cnt+1 are all the relevant patterns */
    /* we need two tokens: pattern number and number of iterations */
    char* str = buf + 1;
    char* start_s = strtok_r(str, " ", &saveptr);
    char* end_s = strtok_r(NULL, " ", &saveptr);
    int32_t start;
    int32_t end;

    if ((NULL == start_s) || (NULL == end_s))
    {
        start = 0;
        end = SRAM_SIZE - 1;
    }
    else
    {
        start = util_atox(start_s);
        end = util_atox(end_s);
    }

    if (start < 0)
        start = (-start);
    if (end < 0)
        end = (-end);

    if (start > SRAM_SIZE - 1)
        start = 0;
    if (end > SRAM_SIZE - 1)
        end = SRAM_SIZE - 1;

    if (start > end)
    {
        start = 0;
        end = SRAM_SIZE - 1;
    }

    start += SRAM_BASE;
    end += SRAM_BASE;

    sram_dump((uint8_t *) start, (uint8_t*) end);
}


/**
 * @brief Prints Monitor usage information.
 * @retval none
 */
static void usage(void)
{
    log_msg("");
    log_msg("The following commands are available:");
    log_msg("=====================================");
    log_msg("%c <start> <end> Dumps SRAM from <start> to <end>", CMD_SRAM_DUMP);
    log_msg("%c   Shows information about running system", CMD_INFO);
    log_msg("%c   Gets actual time", CMD_GET_TIME);
    log_msg("%c <hh> <mm> <sec>  Sets actual time", CMD_SET_TIME);
    log_msg("%c   Resets Controller", CMD_RESET);
    log_msg("%c   Shows this help", CMD_HELP);
    log_msg("");
}


/**
 * @brief Will be used as instruction processor, the over uart received buffer will be analyzed and executed
 * @param buf the input buffer from uart as char stream with blank as delimiter
 * @param pos the number of received characters
 * @retval none
 * @see  switch_cpu_burner() read_data() write_data() last_error() set_trigger_conf() monitor_mram_test()
 * @see sram_dump() print_all_commands() run_tolerance_check() monitor_flash_test() run_adc_test() illu_temp_read() log_msg()
 */
bool monitor_analyse_cmd(char *buf, size_t buflen)
{
    bool rv = true;
    bool cmd_consumed = false;

    for (uint8_t cnt = 0; cnt < buflen; cnt++)
    {
        switch (buf[cnt])
        {
        case CMD_INFO:
            information();
            cmd_consumed = true;
            break;
        case CMD_RESET:
            cmd_consumed = true;
            log_msg("Resetting ioc ...");
            vTaskDelay(10);
            reset_ioc();
            break;
        case CMD_SRAM_DUMP:
            sram_dump_parse(&buf[cnt]);
            cmd_consumed = true;
            break;
        case CMD_HELP:
            usage();
            cmd_consumed = true;
            break;
        case CMD_SEND_THREE_BYTES:
        	// we have to change that anyway
        	// send_three_bytes();
        	cmd_consumed = true;
        	break;
        case CMD_GET_TIME:
            log_msg("CMD_GET_TIME");
            log_msg("Error");
            cmd_consumed = true;
            break;
        case CMD_SET_TIME:

            break;
        case YES:
            break;
        case NO:
            break;
        case BLANK:     /* FALLTHROUGH */
        case TAB:
            break;
        default:
            log_msg(INVALID_CMD);
            rv = false;
            break;
        }

        if (cmd_consumed)
            break;
    }

    return rv;
}



extern uint16_t VCP_DataTx(uint8_t* Buf, uint32_t Len);

/**
 * @gbrief Our monitor loop that reads out of the uart and executes commands
 * @retval none
 */
static void monitor_loop(void* arg)
{
    char* buf = arg;
    uint8_t ch;
    size_t pos = 0;

    information();
    log_msg(PROMPT);
    while (true)
    {
        ch = usart1_getc();

        /* ignore carriage return / newline */
        if ((ch != '\n') || (ch != '\r'))
        {
            if (pos < RXBUFFERSIZE - 1)
            {
                /* assemble buffer */
                buf[pos++] = ch;
            }
        }

        /* echo ch on console */
        if (log_is_task_enabled())
            usart1_putc(ch);
        else
            (void) VCP_DataTx(&ch, 1);

        if (ch == '\r')
        {
            /* zero terminate string */
            buf[pos] = '\0';
            monitor_analyse_cmd(buf, pos);
            pos = 0;
            /* send newline as well */
            log_msg(PROMPT);
        }
    }
}

/**
 * start Monitor task
 * @retval none
 */
bool monitor_init()
{
    static bool is_initialized = false;

    if (!is_initialized)
    {
        // allocate buffer for one line
        unsigned char* buf = pvPortMalloc(RXBUFFERSIZE);
        if (NULL == buf)
        {
            log_msg("%s: pvPortMalloc() failed", __FUNCTION__);
            return false;
        }
        xTaskHandle th=NULL;
        xTaskCreate(monitor_loop, (const char * ) "Monitor", 124, buf, 4,
                &th);
        os_add_tcb(th);

        is_initialized = true;
    }
    return true;
}




/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
